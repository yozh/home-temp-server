/*
code for decoding of URL query params
*/
var urlParams;
(window.onpopstate = function () {
    var match,
    pl     = /\+/g,  // Regex for replacing addition symbol with a space
    search = /([^&=]+)=?([^&]*)/g,
    decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
    query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
	urlParams[decode(match[1])] = decode(match[2]);
})();

function ajax(url, successCallback, errorCallback) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 ) {
            if (xmlhttp.status < 400){
		successCallback(xmlhttp.responseText);
            }
            else if (xmlhttp.status >= 400) {
		errorCallback({
		    status : xmlhttp.status,
		    response : xmlhttp.response
		});
            }
        }
    }

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

var readings = [];

var checkTemp = function (count, callback) {
    var readingCount = 60;
    if (urlParams.count) {
	readingCount = +urlParams.count;
    }
    ajax("/co2/api/reading?limit=" + readingCount, function (response) {
	updateReadings(JSON.parse(response));

	var currentTemp =  readings[0];
	updateDisplayTemp(currentTemp);

	if (callback) {
	    callback()
	}

//	setTimeout(checkTemp, 30000);
    }, function (response) {
//	setTimeout(checkTemp, 30000);
    });
};

var updateDisplayTemp = function (reading) {
    document.getElementById("current-temp").innerHTML = 
	Math.round(reading.temperature);
    document.getElementById("last-update-time").innerHTML = 
	new Date(reading.date).toLocaleString();
}

var chartData = {};
function updateReadings(newReadings) {
    var lastReading = newReadings[0];

    readings = newReadings.sort(function(r1, r2) {
	return r1.date - r2.date;
    });

    var series = readings.map(function (reading) {
	return [reading.date - new Date().getTimezoneOffset() * 60 * 1000, 
		reading.value];
    })

    var options = {
        chart: {
	    renderTo: 'temp-chart',
	    type: 'line'
        },
        title: {
	    text: 'Сейчас ' + lastReading.value + " PPM",
	    useHTML: true
        },
	subtitle: {
	    text: "Последнее обновление: " + new Date(lastReading.date).toLocaleString()
	},
        xAxis: {
	    type: 'datetime'
        },
        yAxis: {
	    title: {
                text: 'CO\u00B2'
	    }
        },
        series: [{
	    name: 'Офис',
		data: series
        }],
	plotOptions: {
	    line: {
		marker: {
		    enabled: false
		}
	    }
	},
	tooltip: {
	    
	}
    };
    var tempChart = new Highcharts.Chart(options);
}

checkTemp();
