const BOT_USERNAME = "detector_reading_bot";
const API_KEY = "649058194:AAGBsn4OOjjB7ks7BVQ0r-w0kG5w0cx1k1I";

const TelegramBot = require('node-telegram-bot-api');
const MongoClient = require('mongodb').MongoClient;
const moment = require('moment');

const dbName = 'reading-bot';
const client = new MongoClient('mongodb://localhost:27017');
let db;

let CHAT_IDS = [];

client.connect(function (err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log("Connected successfully to server");

    db = client.db(dbName);

    const chatsCollection = db.collection('chats');
    chatsCollection.find({}).toArray((err, chats) => {
        CHAT_IDS = chats.map(c => c.id);
        console.log(CHAT_IDS);
    });
});

const bot = new TelegramBot(API_KEY);

const LEVEL_INDICATORS = [
    {
        max: 799,
        indicator: "✅"
    },
    {
        min: 800,
        max: 1199,
        indicator: "⚠️"
    },
    {
        min: 1200,
        indicator: "️‼"
    }
];

function getIndicator(level) {
    let levelIndicator = LEVEL_INDICATORS.filter(li => (!li.min || li.min <= level) && (!li.max || li.max >= level));
    return levelIndicator.length > 0 ? levelIndicator[0].indicator : `[${level}]`;
}


class BotHandler {
    constructor() {
        this.criticalLevelMessageSent = false;
        this.optimalLevelMessageSent = false;
        this.isMuted = false;
    }

    async updateChats(reading) {
        CHAT_IDS.forEach(async chatId => {
            let chat;
            try {
                chat = await bot.getChat(chatId);
            } catch (e) {
                console.log(`Cannot get chat ${chatId}`, e);
            }
            if (chat) {
                if (chat.pinned_message) {
                    if (chat.pinned_message.from.username === BOT_USERNAME) {
                        try {
                            let message = await bot.editMessageText(this.formatMessage(reading), {
                                chat_id: chatId,
                                message_id: chat.pinned_message.message_id,
                                parse_mode: "Markdown"
                            });
                            console.log(message);
                        } catch (e) {
                            console.log(`Error editing pinned message in chat ${chatId}`, e);
                        }
                    }
                } else {
                    try {
                        let message = await bot.sendMessage(chatId, this.formatMessage(reading), {parse_mode: "Markdown"});
                        console.log(message);
                        if (chat.type === 'supergroup') {
                            await bot.pinChatMessage(chatId, message.message_id);
                        } else {
                            bot.sendMessage(chatId, "Я не могу закрепить сообщение в группе, которая не является супергруппой");
                        }
                    } catch (e) {
                        console.log(`Error when sending/pinning message in chat ${chatId}`, e);
                    }
                }
                this.sendCriticalLevelMessage(chatId, reading);
            }
        });
    }

    // TODO: fix for multiple chats
    // TODO: do not send when level is decreasing
    sendCriticalLevelMessage(chatId, reading) {
        if (!this.isMuted) {
            if (!this.criticalLevelMessageSent && reading.value >= LEVEL_INDICATORS[LEVEL_INDICATORS.length - 1].min + 200) {
                bot.sendMessage(chatId, "Мы все умрёём!");
                this.criticalLevelMessageSent = true;
                this.optimalLevelMessageSent = false;
                setTimeout(() => this.criticalLevelMessageSent = false, 15 * 60 * 1000);
            } else if (!this.criticalLevelMessageSent && reading.value >= LEVEL_INDICATORS[LEVEL_INDICATORS.length - 1].min + 100) {
                bot.sendMessage(chatId, "Камон, я начинаю задыхаться!");
                this.criticalLevelMessageSent = true;
                this.optimalLevelMessageSent = false;
                setTimeout(() => this.criticalLevelMessageSent = false, 15 * 60 * 1000);
            } else if (!this.criticalLevelMessageSent && reading.value >= LEVEL_INDICATORS[LEVEL_INDICATORS.length - 1].min) {
                // bot.sendMessage(chatId, "Эй, не пора ли проветрить?");
                bot.sendSticker(chatId, "CAADAgADYwEAAhZ8aAOZHGE-jLPCQgI");
                this.criticalLevelMessageSent = true;
                this.optimalLevelMessageSent = false;
                setTimeout(() => this.criticalLevelMessageSent = false, 15 * 60 * 1000);
            } else if (!this.optimalLevelMessageSent && reading.value < LEVEL_INDICATORS[0].max - 100) {
                bot.sendSticker(chatId, "CAADAgADGQEAAhZ8aAOLsSzIekKUIgI");
                this.optimalLevelMessageSent = true;
                // setTimeout(() => this.optimalLevelMessageSent = false, 180 * 60 * 1000);
            }
        }
    }


    formatMessage(reading) {
        return `${getIndicator(reading.value)} Уровень CO² - ${reading.value} PPM. ` +
            `Последнее обновление ${moment(reading.date).utcOffset(120).format("YYYY-MM-DD HH:mm")}. ` +
            `[График](http://soccer.smiss.ua/co2/chart/).`;
    }

    async addToChat(chatId) {
        bot.sendMessage(chatId, "Привет. Я предоставляю показания датчика CO² при каждом считывании данных. Чтобы не слать их отдельными сообщениями, " +
            "я закреплю первое своё сообщение с показаниями и буду обновлять его.");
        CHAT_IDS.push(chatId);
        return db.collection('chats').insert({id: chatId}).then(err => {
            console.log(err);
        });
    }

    async removeFromChat(chatId) {
        let pos = CHAT_IDS.indexOf(chatId);
        if (pos != -1) {
            CHAT_IDS.splice(pos, 1);
            db.collection('chats').deleteOne({id: chatId});
        }
    }

    mute() {
        this.isMuted = true;
    }

    unmute() {
        this.isMuted = false;
    }

    async setup(app) {
        console.log("Setup bot-handler");
        app.post(`/${API_KEY}/`, (req, res) => {
            console.log("incoming", JSON.stringify(req.body));
            if (req.body.message) {
                if ((req.body.message.text == '/start' || req.body.message.text == `/start@${BOT_USERNAME}`) &&
                    !~CHAT_IDS.indexOf(req.body.message.chat.id)) {
                    this.addToChat(req.body.message.chat.id);
                } else if ((req.body.message.text == '/stop' || req.body.message.text == `/stop@${BOT_USERNAME}`)) {
                    this.removeFromChat(req.body.message.chat.id);
                } else if ((req.body.message.text == '/mute' || req.body.message.text == `/mute@${BOT_USERNAME}`)) {
                    this.mute();
                    bot.sendMessage(req.body.message.chat.id, "Замолкаю на 1 час");
                    setTimeout(this.unmute.bind(this), 60 * 60 * 1000);
                } else if ((req.body.message.text == '/unmute' || req.body.message.text == `/unmute@${BOT_USERNAME}`)) {
                    this.unmute();
                }
            }
            res.end();
        });

        try {
            let result = await bot.setWebHook(`https://soccer.smiss.ua/detectorbot/${API_KEY}`, {'certificate': './smiss-soccer.pem'});
            console.log(result);
            console.log(require('fs').existsSync('./smiss-soccer.pem'));
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports = new BotHandler();
