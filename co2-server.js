var express = require("express");
var bodyparser = require("body-parser");
const {Datastore} = require('@google-cloud/datastore');
const subscriptionReader = require('./co2-subscription-reader');

const BOT_UPDATE_INTERVAL = 60 * 1000;

var app = express();
app.use(bodyparser.json({}));

// setup bot handler
const botHandler = require('./bot-handler');
botHandler.setup(app);

const ds = new Datastore({
    projectId: 'massive-physics-671',
});

subscriptionReader((reading) => {
    saveReading(reading)
});

let lastUpdated = 0;
function saveReading(reading, cb) {
    console.log("Saving reading: " + JSON.stringify(reading));

    ds.save({
        key: ds.key("co2-reading"),
        data: reading
    }, function (err, key) {
        if (typeof cb == "function") {
            cb();
        }
    });

    if (new Date().getTime() - lastUpdated > BOT_UPDATE_INTERVAL) {
        console.log("Updating chat");
        botHandler.updateChats(reading);
        lastUpdated = new Date().getTime();
    }
}


/*
    HTTP interface
*/

app.post("/co2/api/reading", function (req, res) {
    var reading = {
        date: +req.body.date,
        value: +req.body.value
    };

    saveReading(reading, res, () => {
        res.status(201).end();
    });
});

app.get("/co2/api/reading", function (req, res) {
    var query = ds.createQuery(null, 'co2-reading')
        .select(['date', 'value'])
        .order("date", {descending: true});

    if (req.query.limit) {
        query = query.limit(+req.query.limit);
    } else {
        query = query.limit(10);
    }

    query.run(function (err, entities) {

        if (err) {
            console.log(err);
            res.status(500).end(err.message);
            return;
        }

//        console.log(entities);

        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(entities.map(function (item) {
            return item;
        })));
    })
});

app.listen(8081);
