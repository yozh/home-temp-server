var express = require("express");
var bodyparser = require("body-parser");
//var gcloud = require('gcloud');

const {Datastore} = require('@google-cloud/datastore');

var app = express();

//var ds = gcloud.datastore.dataset({
  //  projectId: 'massive-physics-671',
//});
const ds = new Datastore({
    projectId: 'massive-physics-671',
  });

app.use(bodyparser.json({}));

/*
app.post("/temperature", function (req, res) {
    var reading = {
	date: +req.body.date,
	temperature: +req.body.temperature
    };

    ds.save({
	key: ds.key("temp-reading"),
	data: reading
    }, function (err, key) {
	res.status(201).end(JSON.stringify({"key":key}))
    });
});

app.get("/temperature", function (req, res) {
    var query = ds.createQuery(null, 'temp-reading').order("date", {descending:true});
    
    if (req.query.limit) {
        query = query.limit(+req.query.limit);
    } else {
	query = query.limit(10);
    }
    
    query.run(function (err, entities) {

	console.log(entities);

	res.setHeader("Content-Type", "application/json");
	res.end(JSON.stringify(entities.map(function (item) {
	    return item;
	})));
    })
});
*/

const botHandler = require('./bot-handler');
botHandler.setup(app);

setInterval(() => {
    botHandler.updateChats({date: 1548089754, value: 800 + Math.random() * 1000});
}, 5000);

app.listen(8082);
