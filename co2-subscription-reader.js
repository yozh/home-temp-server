const {PubSub} = require('@google-cloud/pubsub');

function listenToCo2Readings(listener) {
    const pubsub = new PubSub();
    const subscription = pubsub.subscription('projects/massive-physics-671/subscriptions/co2-subscription');

    const messageHandler = message => {
        // console.log(`Received message ${message.id}:`);
        let data = Buffer.from(message.data).toString("utf-8");
        // console.log(data);
        // console.log(`Attributes: ${JSON.stringify(message.attributes)}`);

        listener(JSON.parse(data));

        message.ack();
    };

    subscription.on(`message`, messageHandler);
    subscription.on(`error`, (e) => {
        console.warn(`Error when polling pubsub messages`, e);
    });
}

module.exports = listenToCo2Readings;